1. Download the repository.
2. Visit chrome://extensions in your browser.
3. Ensure that the Developer mode checkbox in the top right-hand corner is checked.
4. Click Load unpacked extension… to pop up a file-selection dialog.
5. Navigate to the directory in which you downloaded the repository, and select it.
