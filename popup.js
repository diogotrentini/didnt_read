// Copyright (c) 2014 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

var positives, negatives;

function compileTemplate(keywords, sentiment) {
  var template = '';

  $.each(keywords, function() {
    template +=
      '<div class="keyword ' + sentiment + '">' +
        '<div class="text">' + this + '</div>' +
      '</div>';
  });

  return template;
}

function processKeywords(keywords) {
  var map = keywords.reduce(function (p, c) {
    p[c.text.toLowerCase()] = (p[c.text.toLowerCase()] || 0) + (c.sentiment.score || 0)*100;
    return p;
  }, {});

  var unorderedPositives = {};
  var unorderedNegatives = {};

  for (item in map) {
    if (map[item] > 0) {
      unorderedPositives[item] = map[item];
    } else if (map[item] < 0) {
      unorderedNegatives[item] = map[item];
    }
  }

  var positives = Object.keys(unorderedPositives).sort(function (a, b) {
    if (unorderedPositives[a] < unorderedPositives[b]) {
      return 1;
    } else if (unorderedPositives[a] > unorderedPositives[b]) {
      return -1;
    } else {
      return 0;
    }
  });

  var negatives = Object.keys(unorderedNegatives).sort(function (a, b) {
    if (unorderedNegatives[a] > unorderedNegatives[b]) {
      return 1;
    } else if (unorderedNegatives[a] < unorderedNegatives[b]) {
      return -1;
    } else {
      return 0;
    }
  });

  return { positives: positives, negatives: negatives };
};

function analyzeReviews(reviews) {
  var baseUrl = 'http://gateway-a.watsonplatform.net/calls/text/TextGetRankedKeywords?outputMode=json&sentiment=1&knowledgeGraph=1&apikey=85e62ad889b1b15314bb96cf6387592215231fc5&text=';
  var promises = [];
  var keywords = [];

  $.each(reviews, function() {
    promises.push($.ajax(baseUrl + this));
  });

  $.when.apply($, promises).done(function() {
    $.each(arguments, function() {
      keywords = keywords.concat(this[0].keywords);
    });

    var processedKeywords = processKeywords(keywords);
    var positivesList = compileTemplate(processedKeywords.positives, 'positive');
    var negativesList = compileTemplate(processedKeywords.negatives, 'negative');

    $('.list').html(positivesList + negativesList);
  });
}

function getReviews(productId) {
  var reviews = [];
  var url = "http://api.walmartlabs.com/v1/reviews/" + productId + "?apiKey=5zg6qe74arn5jx8vuzej976e&format=json";

  $.ajax({
    url: url,
    async: false
  }).done(function(data) {
    reviews = $.map(data.reviews, function(val, i) { return val.reviewText });
  });

  return reviews;
}

chrome.runtime.onMessage.addListener(function(request, sender) {
  if (request.action == "getSource") {
    var itemId = $(request.source).find('.js-reviews').data('product-id');
    var productReviews = getReviews(itemId);
    analyzeReviews(productReviews);
  }
});

function onWindowLoad() {
  $('.button').on('click', function() {
    var sentiment = $(this).data('sentiment');

    if (sentiment === 'positive') {
      $('.positive').show();
      $('.negative').hide();
    } else {
      $('.positive').hide();
      $('.negative').show();
    }
  });

  chrome.tabs.executeScript(null, {
    file: "getHTML.js"
  }, function() {
    // If you try and inject into an extensions page or the webstore/NTP you'll get an error
    if (chrome.runtime.lastError) {
      message.innerText = 'There was an error injecting script : \n' + chrome.runtime.lastError.message;
    }
  });
}

window.onload = onWindowLoad;

